import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, precision_score, confusion_matrix
from sklearn.model_selection import GridSearchCV

# Cargar los datos
datos = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Convertir la variable objetivo "quality" en una variable binaria
datos['quality'] = np.where(datos['quality'] > 6.5, 1, 0)

# Seleccionar las columnas que vamos a utilizar en el modelo
X = datos[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = datos['quality']

# Normalizar las características
estandar = StandardScaler()
X = estandar.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Definir los parámetros a buscar con GridSearchCV
parametros = {
    'n_neighbors': range(1, 50),
    'weights': ['uniform', 'distance'],
    'metric': ['euclidean', 'manhattan', 'minkowski']
}

# Crear el modelo
modelo = KNeighborsClassifier()

# Buscar los mejores parámetros con GridSearchCV
grid_search = GridSearchCV(modelo, parametros, cv=5)
grid_search.fit(X_train, y_train)

# Entrenar el modelo con los mejores parámetros
mejor_modelo = grid_search.best_estimator_
mejor_modelo.fit(X_train, y_train)

# Hacer las predicciones
y_pred = mejor_modelo.predict(X_test)

# Calcular las métricas de rendimiento
exactitud = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred, average='weighted')
matriz = confusion_matrix(y_test, y_pred)

print("Exactitud:", exactitud)
print("Precisión:", precision)
print("Matriz de confusión:\n", matriz)
