import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix

# Cargar los datos
datos = pd.read_csv('C:\\Users\\Casa\\TFG\\VinosQualityL.csv')

# Crear una nueva variable "quality_label" basada en la variable "quality"
datos['quality_label'] = datos['quality'].apply(lambda value: 1 if value >= 7 else 0)

# Seleccionar las columnas que vamos a utilizar
X = datos[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = datos['quality_label']

# Normalizar las características
estandar = StandardScaler()
X = estandar.fit_transform(X)

# Dividir los datos en entrenamiento y prueba
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Crear el modelo y entrenarlo
modelo = LogisticRegression(random_state=42)
modelo.fit(X_train, y_train)

# Hacer las predicciones
prediccion = modelo.predict(X_test)

# Calcular la exactitud y la matriz de confusión
exactitud = accuracy_score(y_test, prediccion)
matriz = confusion_matrix(y_test, prediccion)

# Imprimir los resultados
print("Exactitud:", exactitud)
print("Matriz de confusión:\n", matriz)